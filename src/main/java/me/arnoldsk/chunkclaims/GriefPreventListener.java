package me.arnoldsk.chunkclaims;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Objects;

public class GriefPreventListener implements Listener {
    private ChunkClaims plugin;

    GriefPreventListener(ChunkClaims plugin) {
        this.plugin = plugin;
    }

    private void playerAreaEnterMessage(Player player, String chunkOwnedBy) {
        if (chunkOwnedBy.equalsIgnoreCase(player.getName())) {
            plugin.announce(player, ChatColor.GOLD + "Entered your area");
        } else {
            // Show a different color if the player is allowed to bypass the claims
            ChatColor chatColor = plugin.playerHasAllowedName(chunkOwnedBy, player.getName()) ? ChatColor.GOLD : ChatColor.AQUA;

            plugin.announce(player, chatColor + "Area owned by " + chunkOwnedBy);
        }
    }

    private void playerAreaLeaveMessage(Player player, String chunkOwnedBy) {
        if (chunkOwnedBy.equalsIgnoreCase(player.getName())) {
            plugin.announce(player, ChatColor.GOLD + "Left your area");
        } else {
            // Show a different color if the player is allowed to bypass the claims
            ChatColor chatColor = plugin.playerHasAllowedName(chunkOwnedBy, player.getName()) ? ChatColor.GOLD : ChatColor.AQUA;

            plugin.announce(player, chatColor + "Left " + chunkOwnedBy + " owned area");
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Chunk fromChunk = event.getFrom().getChunk();
        Chunk toChunk = Objects.requireNonNull(event.getTo()).getChunk();

        String fromChunkConfigKey = plugin.getChunkConfigKey(fromChunk);
        String toChunkConfigKey = plugin.getChunkConfigKey(toChunk);

        String fromChunkOwnedBy = plugin.getClaimsConfig().getString(fromChunkConfigKey);
        String toChunkOwnedBy = plugin.getClaimsConfig().getString(toChunkConfigKey);

        boolean wasInOwnedChunk = fromChunkOwnedBy != null;
        boolean isInOwnedChunk = toChunkOwnedBy != null;

        if (isInOwnedChunk) {
            if (!wasInOwnedChunk) {
                // Player entered an owned chunk
                playerAreaEnterMessage(player, toChunkOwnedBy);
            } else if (!fromChunkOwnedBy.equalsIgnoreCase(toChunkOwnedBy)) {
                // Player left and entered another owned chunk that is joined
                playerAreaLeaveMessage(player, fromChunkOwnedBy);
                playerAreaEnterMessage(player, toChunkOwnedBy);
            }
        } else {
            if (wasInOwnedChunk) {
                // Player left an owned chunk
                playerAreaLeaveMessage(player, fromChunkOwnedBy);
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        Block blockClicked = event.getClickedBlock();

        if (action == Action.PHYSICAL || action == Action.RIGHT_CLICK_BLOCK) {
            Chunk chunk = Objects.requireNonNull(event.getClickedBlock()).getLocation().getChunk();

            // Allow interacting with crafting blocks and the ender chest
            // Damaged anvil is ignored to prevent intentionally destroying them
            Material blockType = blockClicked.getType();

            if (
                blockType.equals(Material.CRAFTING_TABLE) ||
                blockType.equals(Material.ENCHANTING_TABLE) ||
                blockType.equals(Material.LOOM) ||
                blockType.equals(Material.GRINDSTONE) ||
                blockType.equals(Material.FLETCHING_TABLE) ||
                blockType.equals(Material.SMITHING_TABLE) ||
                blockType.equals(Material.ENDER_CHEST) ||
                blockType.equals(Material.ANVIL) ||
                blockType.equals(Material.CHIPPED_ANVIL)
            ) {
                return;
            }

            // Check if the player is allowed in this chunk
            if (plugin.playerNotAllowed(player, chunk)) {
                if (action == Action.RIGHT_CLICK_BLOCK) {
                    plugin.announce(player, ChatColor.GRAY + "You can't do that here");
                }

                event.setCancelled(true);
                event.setUseInteractedBlock(Event.Result.DENY);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Chunk chunk = event.getBlock().getChunk();

        if (plugin.playerNotAllowed(player, chunk)) {
            plugin.announce(player, ChatColor.GRAY + "You can't break blocks here");
            event.setCancelled(true);
        }
    }

    /**
     * @param event placed block info
     * @implNote will be ignored if onPlayerInteract is active
     */
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Chunk chunk = event.getBlock().getChunk();

        if (plugin.playerNotAllowed(player, chunk)) {
            plugin.announce(player, ChatColor.GRAY + "You can't place blocks here");
            event.setCancelled(true);
        }
    }

    /**
     * @param event emptied bucket info
     * @implNote will be ignored if onPlayerInteract is active
     */
    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        Player player = event.getPlayer();
        Chunk chunk = event.getBlockClicked().getChunk();

        if (plugin.playerNotAllowed(player, chunk)) {
            plugin.announce(player, ChatColor.GRAY + "You can't place water/lava here");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            Chunk chunk = event.getEntity().getLocation().getChunk();

            if (plugin.playerNotAllowed(player, chunk)) {
                plugin.announce(player, ChatColor.GRAY + "You can't damage things here");
                event.setCancelled(true);
            }
        }
    }
}
