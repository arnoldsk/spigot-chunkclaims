package me.arnoldsk.chunkclaims;

import me.arnoldsk.chunkclaims.commands.*;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

public final class ChunkClaims extends JavaPlugin {
    private FileConfiguration config = getConfig();
    private File claimsConfigFile;
    private File allowedConfigFile;
    private FileConfiguration claimsConfig;
    private FileConfiguration allowedConfig;

    @Override
    public void onEnable() {
        // Init configuration
        config.addDefault("enabled", true);
        config.options().copyDefaults(true);
        saveConfig();

        // Custom config
        createClaimsConfig();
        createAllowedConfig();

        // Enable/Disable the plugin based on config
        this.setEnabled(config.getBoolean("enabled"));

        // Add listeners
        getServer().getPluginManager().registerEvents(new GriefPreventListener(this), this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            switch (command.getName().toLowerCase()) {
                // Base claim commands
                case "claim":
                    return new Claim(this).run(sender, command, label, args);
                case "unclaim":
                    return new Unclaim(this).run(sender, command, label, args);
                case "claims":
                    return new Claims(this).run(sender, command, label, args);
                // Player allowance commands
                case "claims-allow":
                    return new ClaimsAllow(this).run(sender, command, label, args);
                case "claims-allowed":
                    return new ClaimsAllowed(this).run(sender, command, label, args);
                case "claims-disallow":
                    return new ClaimsDisallow(this).run(sender, command, label, args);
            }
        } catch (Exception e) {
            sender.sendMessage(ChatColor.DARK_RED + "Error: command failed");
            getLogger().log(Level.SEVERE, e.getMessage());
        }

        return false;
    }

    public void announce(Player player, String message) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
    }

    public String getChunkConfigKey(Chunk chunk) {
        return String.format("%s.%d.%d", chunk.getWorld().getName(), chunk.getX(), chunk.getZ());
    }

    public String getChunkOwnedBy(Chunk chunk) {
        String chunkConfigKey = getChunkConfigKey(chunk);

        return getClaimsConfig().getString(chunkConfigKey);
    }

    public boolean playerHasAllowedName(String ownerName, String checkName) {
        String configKey = String.format("%s.%s", ownerName, checkName.toLowerCase());
        String savedPlayerName = this.getAllowedConfig().getString(configKey);

        return checkName.equalsIgnoreCase(savedPlayerName);
    }

    boolean playerNotAllowed(Player player, Chunk chunk) {
        if (player == null || chunk == null) {
            // Allow placing if anything goes wrong with data
            // Better allow than have trouble placing stuff
            return false;
        } else if (player.isOp()) {
            // Ops have access to all chunks
            return false;
        }

        String chunkOwnedBy = getChunkOwnedBy(chunk);

        if (chunkOwnedBy == null) {
            // Chunk is not owned by anyone, wreak havoc!
            return false;
        }

        // Player owns the chunk
        if (chunkOwnedBy.equalsIgnoreCase(player.getName())) {
            return false;
        }

        // Finally check if the owner has allowed bypass
        return !playerHasAllowedName(chunkOwnedBy, player.getName());
    }

    public Player getOnlinePlayer(Object player) {
        String playerName;
        if (player instanceof Player) {
            playerName = ((Player) player).getName();
        } else {
            playerName = (String) player;
        }

        for (Player onlinePlayer : getServer().getOnlinePlayers()) {
            if (onlinePlayer.getName().equalsIgnoreCase(playerName)) {
                return onlinePlayer;
            }
        }
        return null;
    }

    ////////////////////////
    // Custom configurations
    ////////////////////////

    public FileConfiguration getClaimsConfig() {
        return claimsConfig;
    }

    public FileConfiguration getAllowedConfig() {
        return allowedConfig;
    }

    public void saveClaimsConfig() {
        try {
            getClaimsConfig().save(claimsConfigFile);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Could not save config to " + claimsConfigFile, e);
        }
    }

    public void saveAllowedConfig() {
        try {
            getAllowedConfig().save(allowedConfigFile);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Could not save config to " + allowedConfigFile, e);
        }
    }

    private void createClaimsConfig() {
        String fileName = "claims.yml";
        claimsConfigFile = new File(getDataFolder(), fileName);

        if (!claimsConfigFile.exists()) {
            if (claimsConfigFile.getParentFile().mkdirs()) {
                saveResource(fileName, false);
            } else {
                System.out.println("Could not mkdirs");
            }
        } else {
            System.out.println("Config exists");
        }

        try {
            claimsConfig = new YamlConfiguration();
            claimsConfig.load(claimsConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            // This will always fail but the files will be loaded
            getLogger().log(Level.SEVERE, e.getMessage());
        }
    }

    private void createAllowedConfig() {
        String fileName = "allowed.yml";
        allowedConfigFile = new File(getDataFolder(), fileName);

        if (!allowedConfigFile.exists()) {
            if (allowedConfigFile.getParentFile().mkdirs()) {
                saveResource(fileName, false);
            }
        }

        try {
            allowedConfig = new YamlConfiguration();
            allowedConfig.load(allowedConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            // This will always fail but the files will be loaded
            getLogger().log(Level.SEVERE, e.getMessage());
        }
    }
}
