package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Claim extends CommandBase {
    public Claim(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        Player player = (Player) sender;
        Chunk chunk = player.getLocation().getChunk();
        String chunkOwnedBy = plugin.getChunkOwnedBy(chunk);

        // Check if can claim
        if (chunkOwnedBy != null) {
            if (chunkOwnedBy.equalsIgnoreCase(player.getName())) chunkOwnedBy = "you";
            plugin.announce(player, ChatColor.RED + "This chunk is already owned by " + chunkOwnedBy + ".");
            return true;
        }

        // Should not allow claiming spawn area
        // TODO

        // Save the chunk
        // This should be UUID but for simplicity using name
        plugin.getClaimsConfig().set(plugin.getChunkConfigKey(chunk), player.getName());
        plugin.saveClaimsConfig();

        // Let the player know
        plugin.announce(player, ChatColor.GREEN + "This chunk is now owned by you!");

        return true;
    }
}
