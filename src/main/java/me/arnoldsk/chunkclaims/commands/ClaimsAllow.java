package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClaimsAllow extends CommandBase {
    public ClaimsAllow(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        if (args.length == 0) {
            return false;
        }

        Player player = (Player) sender;
        String allowName = args[0];
        boolean hasAlreadyAllowed = plugin.playerHasAllowedName(player.getName(), allowName);

        if (allowName.equalsIgnoreCase(player.getName())) {
            plugin.announce(player, ChatColor.GRAY + "You played yourself.");
        } else if (hasAlreadyAllowed) {
            plugin.announce(player, ChatColor.GREEN + String.format("Awesome, %s is already allowed", allowName));
        } else {
            Player onlinePlayer = plugin.getOnlinePlayer(allowName);
            String configKey = String.format("%s.%s", player.getName(), allowName.toLowerCase());

            // Check if the player is online to validate player existence
            if (onlinePlayer == null) {
                player.sendMessage("The player has to be online to allow him.");
            } else {
                // Set the name to use online player name for correct capitalization
                allowName = onlinePlayer.getName();

                plugin.getAllowedConfig().set(configKey, allowName.toLowerCase());
                plugin.saveAllowedConfig();
                plugin.announce(player, ChatColor.GREEN + String.format("Allowed %s to bypass your claims", allowName));
            }
        }

        return true;
    }
}
