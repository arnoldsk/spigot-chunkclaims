package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;

public class ClaimsDisallow extends CommandBase {
    public ClaimsDisallow(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        if (args.length == 0) {
            return false;
        }

        Player player = (Player) sender;
        String disallowName = args[0];
        boolean hasAllowed = plugin.playerHasAllowedName(player.getName(), disallowName);

        if (!hasAllowed) {
            plugin.announce(player, ChatColor.GRAY + String.format("%s is already disallowed", disallowName));
        } else {
            String configKey = String.format("%s.%s", player.getName(), disallowName.toLowerCase());

            plugin.getAllowedConfig().set(configKey, null);
            plugin.saveAllowedConfig();

            plugin.announce(player, ChatColor.GREEN + String.format("Disallowed %s from bypassing your claims", disallowName));
        }

        return true;
    }
}
