package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Unclaim extends CommandBase {
    public Unclaim(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        Player player = (Player) sender;
        Chunk chunk = player.getLocation().getChunk();
        String chunkOwnedBy = plugin.getChunkOwnedBy(chunk);

        // Check if it's not claimed
        if (chunkOwnedBy == null) {
            // No action, nothing to send
            plugin.announce(player, ChatColor.GRAY + "This chunk is free already.");
            return true;
        }

        // Check if can unclaim
        if (!chunkOwnedBy.equalsIgnoreCase(player.getName())) {
            plugin.announce(player, ChatColor.RED + "This chunk is not owned by you.");
            return true;
        }

        // Remove from config
        plugin.getClaimsConfig().set(plugin.getChunkConfigKey(chunk), null);
        plugin.saveClaimsConfig();

        // Let the player know
        plugin.announce(player, ChatColor.GREEN + "You don't own this chunk anymore");

        return true;
    }
}
