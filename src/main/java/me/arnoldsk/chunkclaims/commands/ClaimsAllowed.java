package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.MemorySection;
import org.bukkit.entity.Player;

import java.util.Set;

public class ClaimsAllowed extends CommandBase {
    public ClaimsAllowed(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        Player player = (Player) sender;
        // Player name to be used for allowed check
        String playerName = player.getName();
        boolean checkingSelf = true;

        // Check if operator is trying to check other claims
        if (args.length != 0 && player.isOp()) {
            playerName = args[0];

            if (!playerName.equalsIgnoreCase(player.getName())) {
                checkingSelf = false;
            }
        }

        MemorySection allowedSection = (MemorySection) plugin.getAllowedConfig().get(playerName);

        if (allowedSection == null) {
            String suffix = checkingSelf ? "You haven't" : playerName + " hasn't";
            player.sendMessage(String.format("%s allowed anyone.", suffix));
            return true;
        }

        Set<String> allowedNameKeys = allowedSection.getKeys(false);
        StringBuilder commaNameList = new StringBuilder();

        for (String allowedNameKey : allowedNameKeys) {
            String allowedName = allowedSection.getString(allowedNameKey);
            String format = commaNameList.length() == 0 ? "%s" : ", %s";

            commaNameList.append(String.format(format, allowedName));
        }

        String suffix = checkingSelf ? "You have" : playerName + " has";
        player.sendMessage(String.format("%s allowed these players:", suffix));
        player.sendMessage(ChatColor.GRAY + commaNameList.toString());

        return true;
    }
}
