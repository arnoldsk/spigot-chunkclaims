package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.MemorySection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Claims extends CommandBase {
    public Claims(ChunkClaims plugin) {
        super(plugin);
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be called as a player.");
            return true;
        }

        Player player = (Player) sender;
        // Player name to be used for claims check
        String playerName = player.getName();
        boolean checkingSelf = true;

        // Check if operator is trying to check other claims
        if (args.length != 0 && player.isOp()) {
            playerName = args[0];

            if (!playerName.equalsIgnoreCase(player.getName())) {
                checkingSelf = false;
            }
        }

        String worldName = player.getWorld().getName();
        MemorySection worldSection = (MemorySection) plugin.getClaimsConfig().get(worldName);

        if (worldSection == null) {
            player.sendMessage("There are no claimed chunks.");
            return true;
        }

        // Pls close your eyes for this :- )
        Set<String> chunkXKeys = worldSection.getKeys(false);
        Map<String, Object> worldSectionValues = worldSection.getValues(false);
        List<String> chunkMessages = new ArrayList<>();

        for (String chunkXKey : chunkXKeys) {
            MemorySection chunkXSection = (MemorySection) worldSectionValues.get(chunkXKey);
            Set<String> chunkZKeys = chunkXSection.getKeys(false);

            for (String chunkZKey : chunkZKeys) {
                String chunkOwnedBy = chunkXSection.getString(chunkZKey);

                if (chunkOwnedBy == null) {
                    continue;
                }

                if (chunkOwnedBy.equalsIgnoreCase(playerName)) {
                    int chunkX = Integer.parseInt(chunkXKey);
                    int chunkZ = Integer.parseInt(chunkZKey);
                    int chunkWorldX = chunkX << 4;
                    int chunkWorldZ = chunkZ << 4;

                    // Add to messages
                    chunkMessages.add(ChatColor.GRAY + String.format("Chunk at XZ: %s / %s", chunkWorldX, chunkWorldZ));

                    // Draw the particles
                    Chunk chunk = player.getWorld().getChunkAt(chunkX, chunkZ);
                    drawChunkParticleWalls(player, chunk);
                }
            }
        }

        if (chunkMessages.size() == 0) {
            String suffix = checkingSelf ? "You have" : playerName + " has";
            player.sendMessage(String.format("%s no claimed chunks.", suffix));
        } else {
            player.sendMessage(checkingSelf ? "Your claimed chunks:" : String.format("Chunks claimed by %s:", playerName));
            for (String message : chunkMessages) player.sendMessage(message);
        }

        return true;
    }

    private void drawChunkParticleWalls(Player player, Chunk chunk) {
        Location location;
        Particle particle = Particle.DRIP_LAVA;

        int playerBlockY = player.getLocation().getBlockY();
        int locationOffsetY = 4;
        int count = 300;
        double offsetX = 0;
        double offsetY = 2;
        double offsetZ = 0;
        double speed = 0.01;

        location = chunk.getBlock(7, playerBlockY + locationOffsetY, 0).getLocation();
        player.spawnParticle(particle, location, count, offsetX + 3, offsetY, offsetZ, speed);

        location = chunk.getBlock(15, playerBlockY + locationOffsetY, 7).getLocation();
        player.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ + 3, speed);

        location = chunk.getBlock(7, playerBlockY + locationOffsetY, 15).getLocation();
        player.spawnParticle(particle, location, count, offsetX + 3, offsetY, offsetZ, speed);

        location = chunk.getBlock(0, playerBlockY + locationOffsetY, 7).getLocation();
        player.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ + 3, speed);
    }
}
