package me.arnoldsk.chunkclaims.commands;

import me.arnoldsk.chunkclaims.ChunkClaims;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CommandBase implements CommandInterface {
    ChunkClaims plugin;

    CommandBase(ChunkClaims plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean run(CommandSender sender, Command command, String label, String[] args) {
        return false;
    }
}
